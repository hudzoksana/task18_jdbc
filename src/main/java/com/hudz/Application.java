package com.hudz;

import com.hudz.view.MainConsoleMenu;

public class Application {
    public static void main(String[] args) throws Exception {
        new MainConsoleMenu().show();
    }
}
