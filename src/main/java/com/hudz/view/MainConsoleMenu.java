package com.hudz.view;

import com.hudz.controller.ControllerAirline;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.ResourceBundle;

public class MainConsoleMenu extends ConsoleMenu {

  ConsoleMenu consoleMenu;
  Locale locale;
  ResourceBundle bundle;

  private void setMenu() {

    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("4", bundle.getString("4"));
    menu.put("5", bundle.getString("5"));
    menu.put("6", bundle.getString("6"));
    menu.put("7", bundle.getString("7"));
    menu.put("8", bundle.getString("8"));

    menu.put("Q", bundle.getString("Q"));
  }

  public MainConsoleMenu() throws Exception {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MainMenu", locale);
    setMenu();
    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::showDBStructure);
    methodsMenu.put("3", this::workWithAirports);
    methodsMenu.put("4", this::workWithAirlines);

//    methodsMenu.put("2", this::internationalizeMenuUkrainian);
//    methodsMenu.put("3", this::internationalizeMenuEnglish);
  }

  private void workWithAirlines() {
    try {
      consoleMenu = new AirlineConsoleMenu();
    } catch (Exception e) {
      e.printStackTrace();
    }
    this.consoleMenu.show();
  }

  private void workWithAirports() {
    try {
      consoleMenu = new AirportConsoleMenu();
    } catch (Exception e) {
      e.printStackTrace();
    }
    this.consoleMenu.show();
  }


  private void showDBStructure() {
    controller = new ControllerAirline();
    try {
      logger.trace(controller.showDBStruture());
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }


  private void internationalizeMenuUkrainian() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MainMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuEnglish() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MainMenu", locale);
    setMenu();
    show();
  }
}
