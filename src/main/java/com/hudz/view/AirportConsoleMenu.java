package com.hudz.view;

import com.hudz.controller.ControllerAirline;
import com.hudz.controller.ControllerAirport;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.ResourceBundle;

public class AirportConsoleMenu extends ConsoleMenu{

    private void setMenu() {
        this.menu = new LinkedHashMap<>();
        this.menu.put("1", bundle.getString("1"));
        this.menu.put("2", bundle.getString("2"));
        this.menu.put("3", bundle.getString("3"));
        this.menu.put("4", bundle.getString("4"));
        this.menu.put("5", bundle.getString("5"));
        this.menu.put("6", bundle.getString("6"));
    }

    public AirportConsoleMenu() throws Exception {
        locale = new Locale("en");
        controller = new ControllerAirport();
        bundle = ResourceBundle.getBundle("AirportMenu", locale);
        setMenu();
        this.menu.put("Q", bundle.getString("Q"));

        this.methodsMenu.put("1", this::showDBStructure);
        this.methodsMenu.put("2", this::addAirport);
        this.methodsMenu.put("3", this::deleteAirport);
        this.methodsMenu.put("4", this::updateAirport);
        this.methodsMenu.put("5", this::showAirport);
        this.methodsMenu.put("6", this::searchForAirportById);

    }

    private void searchForAirportById() {
        int id = 1;
        try {
            logger.trace(controller.findById(id));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void showAirport() {
        try {
            logger.trace(controller.getTable());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void updateAirport() {

    }

    private void deleteAirport() {
        try {
            controller.deleteEntity(2);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addAirport() {
        try {
            controller.addNewEntity("LWO", "Lviv");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void showDBStructure() {
        try {
            logger.trace(controller.showDBStruture());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void show() {
        super.show();
    }
    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("StringUtilMenu", locale);
        setMenu();
        show();
    }
    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("StringUtilMenu", locale);
        setMenu();
        show();
    }
}
