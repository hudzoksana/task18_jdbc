package com.hudz.controller;

import com.hudz.model.entity.Airport;
import com.hudz.model.service.AirportService;

import java.sql.SQLException;

public class ControllerAirport implements Controller {

    @Override
    public void addNewEntity(String name, String city) throws SQLException {
        Airport airport = new Airport();
        airport.setName(name);
        airport.setCity(city);
        new AirportService().create(airport);
    }

    @Override
    public void deleteEntity(Integer id) throws SQLException {
        airportService.delete(id);
    }

    @Override
    public String getTable() throws SQLException {
        StringBuilder structure = new StringBuilder();
        structure.append("Table Airport:");
        airportService.findAll().forEach(s -> structure.append(s).append('\n'));
        return structure.toString();
    }

    @Override
    public String findById(int id) throws SQLException {
        return airportService.findById(id).toString();
    }

}
