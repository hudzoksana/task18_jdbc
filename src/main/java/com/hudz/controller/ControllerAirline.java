package com.hudz.controller;

import com.hudz.model.entity.Airline;
import com.hudz.model.service.AirlineService;

import java.sql.SQLException;

public class ControllerAirline implements Controller {

    @Override
    public void addNewEntity(String name, String country) throws SQLException {
        Airline airline = new Airline();
        airline.setName(name);
        airline.setCountry(country);
        new AirlineService().create(airline);
    }

    @Override
    public void deleteEntity(Integer id) throws SQLException {
        airlineService.delete(id);
    }

    @Override
    public String getTable() throws SQLException {
        StringBuilder structure = new StringBuilder();
        structure.append("Table Airline:");
        airlineService.findAll().forEach(s -> structure.append(s).append('\n'));
        return structure.toString();
    }

    @Override
    public String findById(int id) throws SQLException {
        return airlineService.findById(id).toString();
    }
}
