package com.hudz.controller;

import com.hudz.model.entity.Airline;
import com.hudz.model.service.AirlineService;
import com.hudz.model.service.AirportService;

import java.sql.SQLException;

public interface Controller {

    AirlineService airlineService = new AirlineService();
    AirportService airportService = new AirportService();

    default String showDBStruture() throws SQLException{
        StringBuilder structure = new StringBuilder();
        structure.append("Table Airline:").append('\n');
        airlineService.findAll().forEach(s -> structure.append(s).append('\n'));
        structure.append("Table Airport:").append('\n');
        airportService.findAll().forEach(s -> structure.append(s).append('\n'));
        return structure.toString();
    }

    void addNewEntity(String name, String country) throws SQLException;

    void deleteEntity(Integer id) throws SQLException;

    String getTable() throws SQLException;

    String findById(int id) throws SQLException;
}
