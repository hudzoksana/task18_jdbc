package com.hudz.model.service;


import com.hudz.model.DAO.implementation.AirportDAOImpl;
import com.hudz.model.entity.Airport;

import java.sql.SQLException;
import java.util.List;

public class AirportService {

    public List<Airport> findAll() throws SQLException {
        return new AirportDAOImpl().findAll();
    }

    public Airport findById(Integer id) throws SQLException {
        return new AirportDAOImpl().findById(id);
    }

    public int create(Airport entity) throws SQLException {
        return new AirportDAOImpl().create(entity);
    }

    public int update(Airport entity) throws SQLException {
        return new AirportDAOImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new AirportDAOImpl().delete(id);
    }

}
