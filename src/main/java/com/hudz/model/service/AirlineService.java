package com.hudz.model.service;

import com.hudz.model.DAO.implementation.AirlineDAOImpl;
import com.hudz.model.entity.Airline;

import java.sql.SQLException;
import java.util.List;

public class AirlineService {

    public List<Airline> findAll() throws SQLException {
        return new AirlineDAOImpl().findAll();
    }

    public Airline findById(Integer id) throws SQLException {
        return new AirlineDAOImpl().findById(id);
    }

    public int create(Airline entity) throws SQLException {
        return new AirlineDAOImpl().create(entity);
    }

    public int update(Airline entity) throws SQLException {
        return new AirlineDAOImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new AirlineDAOImpl().delete(id);
    }

}
