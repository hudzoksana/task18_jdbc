package com.hudz.model.DAO.implementation;

import com.hudz.model.DAO.AirportDAO;
import com.hudz.model.DAO.ConnectionManager;
import com.hudz.model.entity.Airline;
import com.hudz.model.entity.Airport;
import com.hudz.model.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AirportDAOImpl implements AirportDAO {
    private static final String FIND_ALL = "SELECT * FROM Airport";
    private static final String DELETE = "DELETE FROM Airport WHERE id=?";
    private static final String CREATE = "INSERT Airport (name, city) VALUES (?, ?)";
    private static final String UPDATE = "UPDATE Airport SET name=?, city=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM Airport WHERE id=?";

    @Override
    public List<Airport> findAll() throws SQLException {
        List<Airport> airports = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    airports.add((Airport) new Transformer(Airport.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return airports;
    }

    @Override
    public Airport findById(Integer id) throws SQLException {
        Airport entity=null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1,id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity=(Airport) new Transformer(Airport.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(Airport airport) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setString(1,airport.getName());
            ps.setString(2,airport.getCity());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Airport airport) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1,airport.getName());
            ps.setString(2, airport.getCity());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            return ps.executeUpdate();
        }
    }

}
