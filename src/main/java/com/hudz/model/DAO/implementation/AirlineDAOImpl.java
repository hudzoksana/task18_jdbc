package com.hudz.model.DAO.implementation;

import com.hudz.model.DAO.AirlineDAO;
import com.hudz.model.DAO.ConnectionManager;
import com.hudz.model.transformer.Transformer;
import com.hudz.model.entity.Airline;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AirlineDAOImpl implements AirlineDAO {
    private static final String FIND_ALL = "SELECT * FROM Airline";
    private static final String DELETE = "DELETE FROM Airline WHERE id=?";
    private static final String CREATE = "INSERT Airline (name, country) VALUES (?, ?)";
    private static final String UPDATE = "UPDATE Airline SET name=?, country=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM Airline WHERE id=?";

    @Override
    public List<Airline> findAll() throws SQLException {
        List<Airline> airlines = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    airlines.add((Airline) new Transformer(Airline.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return airlines;
    }

    @Override
    public Airline findById(Integer id) throws SQLException {
        Airline entity=null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1,id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity=(Airline) new Transformer(Airline.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(Airline airline) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setString(1,airline.getName());
            ps.setString(2,airline.getCountry());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Airline airline) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1,airline.getName());
            ps.setString(2, airline.getCountry());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            return ps.executeUpdate();
        }
    }

}
