package com.hudz.model.DAO;

import com.hudz.model.entity.Airport;

public interface AirportDAO extends GeneralDAO<Airport,Integer> {

}
