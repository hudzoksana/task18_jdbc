package com.hudz.model.DAO;

import com.hudz.model.entity.Airline;

public interface AirlineDAO extends GeneralDAO <Airline, Integer> {

}
