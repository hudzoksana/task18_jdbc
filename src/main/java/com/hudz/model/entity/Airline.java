package com.hudz.model.entity;

import com.hudz.model.DAO.annotation.Column;
import com.hudz.model.DAO.annotation.PrimaryKey;
import com.hudz.model.DAO.annotation.Table;

import java.util.Objects;

@Table(name = "Airline")
public class Airline {

    @PrimaryKey
    @Column (name = "id")
    private int id;
    @Column (name = "name")
    private String name;
    @Column (name = "country")
    private String country;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Airline(int id, String name, String country) {
        this.id = id;
        this.name = name;
        this.country = country;
    }

    public Airline() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Airline)) return false;
        Airline airline = (Airline) o;
        return getId() == airline.getId() &&
                Objects.equals(getName(), airline.getName()) &&
                Objects.equals(getCountry(), airline.getCountry());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getCountry());
    }

    @Override
    public String toString() {
        return "Airline{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
