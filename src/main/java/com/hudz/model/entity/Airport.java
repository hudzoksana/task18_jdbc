package com.hudz.model.entity;

import com.hudz.model.DAO.annotation.Column;
import com.hudz.model.DAO.annotation.PrimaryKey;
import com.hudz.model.DAO.annotation.Table;

import java.util.Objects;

@Table(name = "Airport")
public class Airport {

    @PrimaryKey
    @Column (name = "id")
    private int id;
    @Column (name = "name")
    private String name;
    @Column (name = "city")
    private String city;

    public Airport() {
    }

    public Airport(int id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Airport)) return false;
        Airport airport = (Airport) o;
        return getId() == airport.getId() &&
                Objects.equals(getName(), airport.getName()) &&
                Objects.equals(getCity(), airport.getCity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getCity());
    }

    @Override
    public String toString() {
        return "Airport{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
