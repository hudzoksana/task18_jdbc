package com.hudz.model.entity;

import com.hudz.model.DAO.annotation.Column;
import com.hudz.model.DAO.annotation.PrimaryKey;
import com.hudz.model.DAO.annotation.Table;

import java.util.Objects;

@Table(name = "boarding pass")
public class BoardingPass {
    @PrimaryKey
    @Column(name = "id")
    private int id;

    @Column(name = "flightId")
    private int flightId;

    @Column(name = "boardingNo")
    private int boardingNo;

    @Column(name = "seatNo")
    private int seatNo;

    public BoardingPass() {
    }

    public BoardingPass(int id, int flightId, int boardingNo, int seatNo) {
        this.id = id;
        this.flightId = flightId;
        this.boardingNo = boardingNo;
        this.seatNo = seatNo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public int getBoardingNo() {
        return boardingNo;
    }

    public void setBoardingNo(int boardingNo) {
        this.boardingNo = boardingNo;
    }

    public int getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(int seatNo) {
        this.seatNo = seatNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BoardingPass)) return false;
        BoardingPass that = (BoardingPass) o;
        return getId() == that.getId() &&
                getFlightId() == that.getFlightId() &&
                getBoardingNo() == that.getBoardingNo() &&
                getSeatNo() == that.getSeatNo();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFlightId(), getBoardingNo(), getSeatNo());
    }

    @Override
    public String toString() {
        return "BoardingPass{" +
                "id=" + id +
                ", flightId=" + flightId +
                ", boardingNo=" + boardingNo +
                ", seatNo=" + seatNo +
                '}';
    }
}
